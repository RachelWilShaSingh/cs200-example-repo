#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    // I've added a comment
}

void Program2()
{
    cout << "Here's a cout" << endl;
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
	cout << "Edited on my computer" << endl;
	cout << "Gonna copy paste this whole file." << endl;
}

void Program6()
{
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
